# Sumário <!-- omit in toc -->

- [Linux](#linux)
- [LPI](#lpi)
  - [Essentials](#essentials)
  - [LPIC-1](#lpic-1)
- [Pyhton](#pyhton)
- [AWS](#aws)
  - [Cloud Practitioner](#cloud-practitioner)
  - [Solutions Architect Associate](#solutions-architect-associate)
  - [Developer Associate](#developer-associate)
  - [SysOps Administrator Associate](#sysops-administrator-associate)
- [Ansible](#ansible)
- [Docker](#docker)
- [Kubernetes](#kubernetes)
- [Git](#git)
- [Markdown & Mermaid](#markdown--mermaid)
- [Terraform](#terraform)
- [Hadoop](#hadoop)
  - [Sqoop & Importação de dados](#sqoop--importação-de-dados)
  - [Ranger](#ranger)
  - [HBase](#hbase)
- [Amazon](#amazon)
- [HashiCorp](#hashicorp)
- [IBM](#ibm)
- [Mozilla](#mozilla)
- [Segurança](#segurança)
- [Sites](#sites)
- [Ferramentas](#ferramentas)
- [Reportagens e Artigos](#reportagens-e-artigos)
- [Tutoriais e Passo-a-Passo](#tutoriais-e-passo-a-passo)
- [Cursos On-Line](#cursos-on-line)
- [Outros compilados](#outros-compilados)

---

Para colaborar adicionando conteúdo veja [estes passos](colabore.md).

---

## Linux

- [Vantagens e desvantagens da sua utilização do SUDO](https://help.ubuntu.com/community/RootSudo).

## LPI

- [LPI Learning](https://learning.lpi.org/en/)
- [Free Training Materials](https://learning.lpi.org/en/learning-materials/learning-materials/)

### Essentials

- [Linux Essentials Oficial](https://learning.lpi.org/en/learning-materials/010-160/)
- [Curso Linux Essentials](https://lcnsqr.com/curso-linux-essentials)

### LPIC-1

- [LPIC-1 Exam 101 Oficial](https://learning.lpi.org/en/learning-materials/101-500/)
- [A roadmap for LPIC-1](https://developer.ibm.com/tutorials/l-lpic1-map/)

---

## Pyhton

- [An A-Z of useful Python tricks](https://medium.com/free-code-camp/an-a-z-of-useful-python-tricks-b467524ee747)
- [20 Python Snippets You Should Learn Today](https://medium.com/better-programming/20-python-snippets-you-should-learn-today-8328e26ff124)
- [30 seconds of Python](https://github.com/30-seconds/30-seconds-of-python)
- [10 Smooth Python Tricks For Python Gods](https://towardsdatascience.com/10-smooth-python-tricks-for-python-gods-2e4f6180e5e3)
- [Dispatch Tables in Python](https://medium.com/better-programming/dispatch-tables-in-python-d37bcc443b0b)
- [The ultimate guide to writing better Python code](https://towardsdatascience.com/the-ultimate-guide-to-writing-better-python-code-1362a1209e5a)
- [Become a Python “One-Liners” Specialist](https://towardsdatascience.com/become-a-python-one-liners-specialist-8c8599d6f9c5)
- [Modern Functions in Python 3 - Funções com Tipos](https://tech.gadventures.com/modern-functions-in-python-3-80208c44ce47)
- [Make your Python Code Bug free - Type Checking](https://medium.com/@gaganmanku96/make-your-python-code-bug-free-ce917d0705b5)
- [Stop Using Square Bracket Notation to Get a Dictionary’s Value in Python](https://medium.com/better-programming/stop-using-square-bracket-notation-to-get-a-dictionarys-value-in-python-c617f6ea15a3)
- [How to reverse a string in Python?](https://medium.com/swlh/how-to-reverse-a-string-in-python-66fc4bbc7379)
- [Change The Way You Write Python Code With One Extra Character](https://medium.com/swlh/change-the-way-you-write-python-code-with-one-extra-character-6665b73803c1)
- [Python Logging Basics](https://medium.com/dev-genius/python-logging-basics-458ad969e850)
- [Customize the Django Admin With Python](https://realpython.com/customize-django-admin-python/)
- [Variables and memory addresses in Python](https://medium.com/@daniel.tooke/variables-and-memory-addresses-in-python-6d96d672ed3d)
- [Singletons and interning in Python](https://medium.com/@daniel.tooke/singletons-and-interning-in-python-328413db334)
- [Will Python intern my string?](https://medium.com/@daniel.tooke/will-python-intern-my-string-94ea9efc18b2)
- [Delete delete delete! How Python collects your garbage for you](https://medium.com/@daniel.tooke/delete-delete-delete-how-python-collects-your-garbage-for-you-ae2c7e9ad84f)
- [Garbage collection in Python: things you need to know](https://rushter.com/blog/python-garbage-collector/)
- [Python Object Graphs](https://mg.pov.lt/objgraph/)
- [Graphviz - Graph Visualization Software](https://www.graphviz.org/)
- [Python Objects Part III: String Interning](https://medium.com/@bdov_/https-medium-com-bdov-python-objects-part-iii-string-interning-625d3c7319de)
- [Making Data Trees in Python](https://medium.com/swlh/making-data-trees-in-python-3a3ceb050cfd)
- [Python Optimizations — Interning](https://levelup.gitconnected.com/python-optimizations-216205001b83)
- [Do Not Use “+” to Join Strings in Python](https://towardsdatascience.com/do-not-use-to-join-strings-in-python-f89908307273)
- [Stop Abusing *args and **kwargs in Python](https://medium.com/better-programming/stop-abusing-args-and-kwargs-in-python-560ce6645e14)
- [3 Insane Secret Weapons for Python](https://towardsdatascience.com/the-3-secret-weapons-that-changed-my-python-editor-forever-c99f7b2e0084)
- [Python — From Intermediate to Superhero](https://blog.usejournal.com/python-from-intermediate-to-superhero-1a86e518bb77)
- [How to use the easiest GUI of your life in Python](https://codeburst.io/how-to-use-the-easiest-gui-of-your-life-in-python-d3762270a2a0)
- [Building Tools with Python](https://medium.com/@javalianimesh/building-tools-with-python-5e38fe348a47)
- [Python Dictionary Operations](https://medium.com/dev-genius/python-dictionary-operations-68e19b08ea30)
- [Lambdas in Python — 4 Practical Examples](https://medium.com/swlh/lambdas-in-python-4-practical-examples-25afe0e44f88)
- [Memory Efficient Functions with Python Generators in 5 Minutes](https://medium.com/@erdemisbilen/memory-efficient-functions-with-python-generators-in-5-minutes-13ccfdfaa89a)
- [The Top 7 Best Github Repositories to Learn Python](https://towardsdatascience.com/top-7-repositories-on-github-to-learn-python-44a3a7accb44)
- [Efficiently Checking for an Empty List in Python](https://medium.com/swlh/efficiently-checking-for-an-empty-list-in-python-76b76099fbd3)
- [Organizing your Python Code](https://medium.com/@k3no/organizing-your-python-code-ca5445843368)
- [Clean Code in Python](https://medium.com/dev-genius/clean-code-in-python-8251eea292fa)

---

## AWS

- [How I passed two AWS certifications in less than two months.](https://medium.com/@ravindraprasad/how-i-passed-two-aws-certifications-in-two-months-511c484d0d0c)
- [AWS Lambda abuse](https://luminousmen.com/post/aws-lambda-abuse)

### Cloud Practitioner

- [AWS Certified Cloud Practitioner Training 2019 - A Free 4-hour Video Course](https://www.freecodecamp.org/news/aws-certified-cloud-practitioner-training-2019-free-video-course/)

### Solutions Architect Associate

- [Minhas anotações](cloud/aws/certificacoes/arquiteto/MAIN.md)
- <!-- Continuar da questão 11 a anotação do que precisa estudar. -->
- [The Ultimate AWS Solutions Architect Certification Guide](https://towardsdatascience.com/the-ultimate-aws-solutions-architect-certification-guide-56c21d4078ed)
- [Pass the AWS Certified Solutions Architect Exam with This FREE 10-Hour Course](https://www.freecodecamp.org/news/pass-the-aws-certified-solutions-architect-exam-with-this-free-10-hour-course/)

### Developer Associate

- [AWS Certified Developer - Associate 2020 (PASS THE EXAM!)](https://www.youtube.com/watch?v=RrKRN9zRBWs)

### SysOps Administrator Associate

- [Pass the AWS SysOps Administrator Associate Exam With This Free 14-Hour Course](https://www.freecodecamp.org/news/aws-sysops-adminstrator-associate-certification-exam-course/)

---

## Ansible

- [10 módulos Ansible que você precisa conhecer](https://opensource.com/article/19/9/must-know-ansible-modules?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CxUyAAK)
- [Ansible for Dummies #0 - The Basics](https://ssh.guru/ansible-for-dummies-0-the-basics/)
- [Ansible for Dummies #1 - Playbooks](https://ssh.guru/ansible-for-dummies-1-playbooks/)

## Docker

- [Docker](https://docs.docker.com)
- [Docker Definitivo](https://dockerdefinitivo.com)
- [The Docker Handbook](https://www.freecodecamp.org/news/the-docker-handbook/)

## Kubernetes

- [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
- [Como codar um Kubernetes Operator em Go - Live com Ellen Korbes](https://www.youtube.com/watch?v=Vt7Eg4wWNDw)
- [The Kubernetes Handbook](https://www.freecodecamp.org/news/the-kubernetes-handbook/#the-full-picture)

## Git

- [GIT - Documentação](https://git-scm.com/doc)
- [Manual | Primeiros passos](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Sobre-Controle-de-Versão)
- [Understanding the GitHub flow](https://guides.github.com/introduction/flow/)
- [Visual Git Cheat Sheet](http://ndpsoftware.com/git-cheatsheet.html)
- [13 dicas do Git para o 13º aniversário do Git](https://opensource.com/article/18/4/git-tips)
- [Gerencie sua agenda diária com o Git](https://opensource.com/article/19/4/calendar-git?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CrNZAA0)
- [writing proper git commits](https://dev.to/technolaaji/writing-proper-git-commits-4g06)
- [4 Git scripts I can't live without](https://opensource.com/article/20/4/git-extras)
- [6 best practices for teams using Git](https://opensource.com/article/20/7/git-best-practices)
- [Git - Guia Prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html)

## Markdown & Mermaid

- [Markdown User Guide](http://fletcher.github.io/peg-multimarkdown)
- [Tutorial Markdown](https://www.markdowntutorial.com)
- [Converta arquivos Markdown em documentos do processador de texto usando o pandoc](https://opensource.com/article/19/5/convert-markdown-to-word-pandoc)

## Terraform

- [Learn to provision infrastructure with HashiCorp Terraform](https://learn.hashicorp.com/terraform)
- [Import Terraform configuration](https://learn.hashicorp.com/terraform/state/import?utm_source=WEBSITE&utm_medium=WEB_BLOG&utm_offer=ARTICLE_PAGE)
- [CDK for Terraform: Enabling Python & TypeScript Support](https://www.hashicorp.com/blog/cdk-for-terraform-enabling-python-and-typescript-support/)

---

## Hadoop

### Sqoop & Importação de dados

- [Documentação: sqoop-import-mainframe](https://sqoop.apache.org/docs/1.4.7/SqoopUserGuide.html#_literal_sqoop_import_mainframe_literal)
- [Aprenda Sqoop Import Mainframe Tool - Sintaxe e Exemplos](https://data-flair.training/blogs/sqoop-import-mainframe/)
- [Descarregando dados de mainframe no Hadoop](https://community.cloudera.com/t5/Community-Articles/Offloading-Mainframe-Data-into-Hadoop/ta-p/246129)
- [Descarregando dados de mainframe no Hadoop: 4 coisas que você precisa saber](https://bigstep.com/blog/offloading-mainframe-data-hadoop-4-things-need-know)
- [Serde for Cobol Layout to Hive table](https://rbheemana.github.io/Cobol-to-Hive/)
- [Big Iron to Big Data: Mainframe to Hadoop with Apache Sqoop](https://blog.syncsort.com/2014/06/big-data/big-iron-big-data-mainframe-hadoop-apache-sqoop/)

### Ranger

- [Práticas recomendadas na autorização de HDFS com o Apache Ranger](https://blog.cloudera.com/best-practices-in-hdfs-authorization-with-apache-ranger/)
- [Discussão sobre políticas do Ranger para HDFS](https://pierrevillard.com/2018/08/14/discussion-around-ranger-policies-for-hdfs/)
- [Umask - Wikipedia](https://pt.wikipedia.org/wiki/Umask)

### HBase

- [Uma visão aprofundada da arquitetura do HBase](https://mapr.com/blog/in-depth-look-hbase-architecture/)
- [Diretrizes para o design do esquema HBase](https://mapr.com/blog/guidelines-hbase-schema-design/)
- [Banco de dados HBase e MapR: projetado para distribuição, escala e velocidade](https://mapr.com/blog/hbase-and-mapr-db-designed-distribution-scale-and-speed/)

---

## Amazon

- [Webinars 2019 - AWS](https://aws.amazon.com/pt/webinars-2019/?&sc_channel=psm&sc_campaign=awswebinarondemandptb&sc_publisher=LI&sc_category=webinar&sc_country=BR&sc_geo=LATAM&sc_outcome=ln&sc_detail=800x400&sc_content=Webinar_On_Demand_PTB&sc_segment=pt_2&sc_medium=FIELD-P|LI|Social-P|All|LD|Webinar|Solution|BR|PT|Sponsored%20Content|xx|Webinar-on-Demand-PT)

## HashiCorp

- [Hashicorp](https://learn.hashicorp.com)
- [Instruqt (Estilo Katacoda)](https://play.instruqt.com/hashicorp)

## IBM

- [IBM Coder Community](https://ibmcoders.influitive.com/challenges)

## Mozilla

- [Mozilla Developer Network](https://developer.mozilla.org/pt-BR/)

---

## Segurança

- [SSH.COM](https://www.ssh.com/)
- [A cyber attack short story](https://blog.ssh.com/a-cyberattack-short-story)
- [Data breaches have a long-lasting impact](https://blog.ssh.com/breach_consequencies_are_long_lasting)

---

## Sites

- [ServerTheHome](https://www.servethehome.com)
- [Linux Descomplicado](https://www.linuxdescomplicado.com.br)

---

## Ferramentas

- [VFsync - Virtual File Synchronization](https://vfsync.org/index.html)
- [Bitwarden - Cofre de Senhas](https://bitwarden.com)

---

## Reportagens e Artigos

- [Oracle apresenta supercomputador com 1060 Raspberry Pi na OOW](https://www.servethehome.com/oracle-shows-1060-raspberry-pi-supercomputer-at-oow/)
- [Duet Display agora está disponível também para Android](https://olhardigital.com.br/noticia/duet-display-agora-esta-disponivel-tambem-para-android/91056)
- [Como instalar o Ubuntu Touch no seu celular (Linux de bolso)](https://sempreupdate.com.br/como-instalar-o-ubuntu-touch-no-seu-celular-linux-de-bolso/)
- [A Beginners Guide To Cron Jobs](https://www.ostechnix.com/a-beginners-guide-to-cron-jobs/)
- [28 fatos sobre o Linux em seu aniversário de 28 anos](https://www.redhat.com/en/blog/28-facts-about-linux-its-28th-birthday?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CvuUAAS)
- [Como mudar a cor do seu terminal Linux](https://opensource.com/article/19/9/linux-terminal-colors)
- [Como o Linux chegou ao mainframe](https://opensource.com/article/19/9/linux-mainframes-part-1?utm_medium=Email&utm_campaign=weekly&sc_cid=7013a000002CxUyAAK)
- [30 segundos para aprender algo novo diariamente (JS, Python e PHP)](https://medium.com/code-prestige/30-segundos-para-aprender-algo-novo-diariamente-js-python-e-php-4a34d1433e82)
- [How I Went From 0 to 70k Subscribers on YouTube in 1 Year – And How Much Money I Made](https://www.freecodecamp.org/news/how-to-grow-your-youtube-channel/)

---

## Tutoriais e Passo-a-Passo

- [Construindo pipelines de CI / CD com Jenkins](https://opensource.com/article/19/9/intro-building-cicd-pipelines-jenkins)
- [Criando uma aplicação com NodeJS, usando Express, Mongo, Docker e Babel](https://emersonbroga.com/passo-a-passo-criando-uma-aplicacao-com-nodejs-usando-express-mongo-docker-e-babel/)
- [Django On Kubernetes - o mais conciso possível](https://labs.meanpug.com/django-on-kubernetes-as-concisely-as-possible/)
- [Métricas de aplicativo personalizadas com Django, Prometheus e Kubernetes](https://labs.meanpug.com/custom-application-metrics-with-django-prometheus-and-kubernetes/)
- [Um guia para iniciantes na construção de pipelines DevOps com ferramentas de código aberto](https://opensource.com/article/19/4/devops-pipeline)
- [Cloud Treinamentos - Vídeos no YouTube](https://www.youtube.com/c/CloudTreinamentos/playlists)
- [How to build a Quiz app using React and TypeScript](https://www.freecodecamp.org/news/how-to-build-a-quiz-app-using-react-and-typescript/)
- [Build Your First Python Project in This Free Course: a Text-Based Adventure Game](https://www.freecodecamp.org/news/your-first-python-project/)

---

## Cursos On-Line

- [WebFor - Curso AWS Certified Solutions Architect](https://webfor.com.br/curso-aws-certified-solutions-architect/)
- [Whizlabs - Cursos diversos e testes práticos](https://www.whizlabs.com/)
- [MapR Academy](https://mapr.com/training/courses/)
- [Digital Innovation One](https://digitalinnovation.one/)
- [Instruqt](https://play.instruqt.com/)
- [freeCodeCamp.org](https://www.freecodecamp.org/)
- [30 seconds of code](https://www.30secondsofcode.org/)
- [Take Your Python Skills to the Next Level With This Free 6-Hour Video Course](https://www.freecodecamp.org/news/intermediate-python-course/)

---

## Outros compilados

- [Diego Pacheco - DevOps Learning the hard way](https://trello.com/b/ZFVZz4Cd/devops-learning-the-hard-way)
- [Andreas Kretz - The Data Engineering Cookbook](https://github.com/andkret/Cookbook)

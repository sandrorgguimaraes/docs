<img src="https://www.mettafora.com.br/wp-content/uploads/2017/10/istock_000011869612small1-copy-800-x-600.jpg" alt="docs" width="100" height="75"/>

# docs

Um local para organizar anotações, documentações relevantes e demais informações úteis... ou não ;).

## Começando

Para ter uma cópia desse material em seu computador clone este repositório utilizando um dos comandos abaixo:

- Via SSH

```bash
git clone git@gitlab.com:sandrorgguimaraes/docs.git
```

- Via HTTPS

```bash
git clone https://gitlab.com/sandrorgguimaraes/docs.git
```

## Mantenha-se atualizado

Use regularmente o comando abaixo, no diretório criado quando você clonou este repositório.

```bash
git pull
```

Para colaborar com este projeto, veja [estes passos](colabore.md).

> TODO: ver este site depois https://marketplace.visualstudio.com/items?itemName=bierner.emojisense
# AWS Certified Solutions Architect Associate <!-- omit in toc -->

- [Links importantes](#links-importantes)
- [Habilidades validadas na certificação](#habilidades-validadas-na-certificação)
- [Conhecimento e experiência recomendados](#conhecimento-e-experiência-recomendados)
- [Principais Domínios](#principais-domínios)
  - [Domain 1: Projete arquiteturas resilientes](#domain-1-projete-arquiteturas-resilientes)
  - [Domain 2: Projete arquiteturas de alto desempenho](#domain-2-projete-arquiteturas-de-alto-desempenho)
  - [Domain 3: Projete arquiteturas e aplicativos seguros](#domain-3-projete-arquiteturas-e-aplicativos-seguros)
  - [Domain 4: Projetar arquiteturas com custo otimizado](#domain-4-projetar-arquiteturas-com-custo-otimizado)
- [Conteúdo](#conteúdo)
  - [Modulo AWS Review](#modulo-aws-review)

## Links importantes

- [Site Oficial](https://aws.amazon.com/pt/certification/certified-solutions-architect-associate/)
- [Guia de aceleração da AWS: Para arquitetos e engenheiros de soluções em nuvem da AWS](https://d1.awsstatic.com/training-and-certification/ramp-up-guides/RampUp_Architect_062020.pdf)

## Habilidades validadas na certificação

- Demonstrar eficazmente conhecimento sobre como arquitetar e implantar aplicativos seguros e robustos em tecnologias da AWS.
- Definir uma solução usando princípios de projeto arquitetônico de acordo com requisitos dos clientes.
- Fornecer orientação sobre implementação com base nas melhores práticas para a empresa durante todo o ciclo de vida do projeto

## Conhecimento e experiência recomendados

- Experiência prática usando serviços de computação, redes, armazenamento e produtos da AWS de banco de dados
Experiência prática com os serviços de implantação e gerenciamento da AWS
- Capacidade de identificar e definir requisitos técnicos para aplicativos baseados na AWS
- Capacidade de identificar quais serviços da AWS cumprem um determinado requisito técnico
- Conhecimento de melhores práticas recomendadas para criação de aplicativos seguros e confiáveis na plataforma da AWS
- Compreensão dos princípios básicos da arquitetura de criação na Nuvem AWS
- Compreensão da infraestrutura global da AWS
- Compreensão das tecnologias de rede relacionadas à AWS
- Compreensão dos recursos e ferramentas de segurança fornecidos pela AWS e de como se relacionam com serviços tradicionais

## Principais Domínios

| Domain | % of Examination |
|--------|:----------------:|
| Domain 1: Design Resilient Architectures | 30% |
| Domain 2: Design High-Performing Architectures | 28% |
| Domain 3: Design Secure Applications and Architectures | 24% |
| Domain 4: Design Cost-Optimized Architectures | 18% |
| **TOTAL** | **100%** |

### Domain 1: Projete arquiteturas resilientes

1. Projete uma solução de arquitetura multicamada
2. Projete arquiteturas altamente disponíveis e / ou tolerantes a falhas
3. Projetar mecanismos desacoplados usando os serviços da AWS
4. Escolha armazenamento resiliente apropriado

### Domain 2: Projete arquiteturas de alto desempenho

1. Identifique soluções de computação elásticas e escalonáveis ​​para uma carga de trabalho
2. Select high-performing and scalable storage solutions for a workload
3. Select high-performing networking solutions for a workload
4. Choose high-performing database solutions for a workload

### Domain 3: Projete arquiteturas e aplicativos seguros

1. Crie acesso seguro aos recursos da AWS
2. Projetar camadas de aplicativos seguros
3. Selecione as opções apropriadas de segurança de dados

### Domain 4: Projetar arquiteturas com custo otimizado

1. Identifique soluções de armazenamento econômicas
2. Identifique serviços de computação e banco de dados econômicos
3. Projete arquiteturas de rede com custo otimizado

## Conteúdo

### Modulo AWS Review

1. [Introdução à nuvem da AWS](modulo_00.md#1-introdução-à-nuvem-da-aws)
2. [Cenários de nuvem](modulo_00.md#2-cenários-de-nuvem)
3. [Visão Geral da Infraestrutura](modulo_00.md#3-visão-geral-da-infraestrutura)
4. [Introdução aos serviços básicos da AWS](modulo_00.md#4-introdução-aos-serviços-básicos-da-aws)
5. [Wrap-up](modulo_00.md#5-wrap-up)

# Modulo 00 - AWS Review <!-- omit in toc -->

- [1. Introdução à nuvem da AWS](#1-introdução-à-nuvem-da-aws)
  - [O que exatamente é a computação em nuvem](#o-que-exatamente-é-a-computação-em-nuvem)
  - [Os três modelos de computação em nuvem](#os-três-modelos-de-computação-em-nuvem)
  - [Benefícios da computação em nuvem da AWS](#benefícios-da-computação-em-nuvem-da-aws)
  - [Pilha de serviços da AWS](#pilha-de-serviços-da-aws)
  - [AWS Foundation Services](#aws-foundation-services)
  - [Serviços não gerenciados vs. serviços gerenciados](#serviços-não-gerenciados-vs-serviços-gerenciados)
- [2. Cenários de nuvem](#2-cenários-de-nuvem)
- [3. Visão Geral da Infraestrutura](#3-visão-geral-da-infraestrutura)
- [4. Introdução aos serviços básicos da AWS](#4-introdução-aos-serviços-básicos-da-aws)
- [5. Wrap-up](#5-wrap-up)
- [Fontes](#fontes)

## 1. Introdução à nuvem da AWS

### O que exatamente é a computação em nuvem

A computação em nuvem é a entrega de recursos de TI sob demanda por meio da Internet com definição de preço de pagamento conforme o uso. Em vez de comprar, ter e manter datacenters e servidores físicos, você pode acessar serviços de tecnologia, como capacidade computacional, armazenamento e bancos de dados, conforme a necessidade, usando um provedor de nuvem como a Amazon Web Services (AWS).

A computação em nuvem permite que você pare de pensar em sua infraestrutura como hardware e, em vez disso, pense nela (e use-a) como software (Serviço).

### Os três modelos de computação em nuvem

- Infraestrutura como serviço (IaaS)

O IaaS contém os componentes básicos da IT na nuvem. Normalmente, o IaaS oferece acesso a recursos de rede, computadores (virtuais ou em hardware dedicado) e espaço de armazenamento de dados. O IaaS **oferece o mais alto nível de flexibilidade e controle de gerenciamento sobre os recursos de TI**. Ele é o tipo de computação mais semelhante aos recursos existentes de TI, já conhecidos por vários departamentos e desenvolvedores de TI.

- Plataforma como serviço (PaaS)

Com o PaaS, você não precisa mais gerenciar a infraestrutura subjacente (geralmente, hardware e sistemas operacionais) **e pode manter o foco na implantação e no gerenciamento de aplicativos**. Dessa forma, você fica mais eficiente, pois não precisa se preocupar com aquisição de recursos, planejamento de capacidade, manutenção de software, correções ou qualquer outro tipo de trabalho genérico repetitivo necessário para a execução dos aplicativos.

- Software como serviço (SaaS)

O SaaS **oferece um produto completo, executado e gerenciado pelo provedor de serviços**. Na maioria dos casos, quando as pessoas mencionam SaaS, estão falando de aplicativos de usuários finais (como e-mail baseado na web). Com uma oferta de SaaS, você não precisa pensar sobre a manutenção do serviço ou o gerenciamento da infraestrutura subjacente. Você só precisa se preocupar sobre como utilizará esse software específico.

### Benefícios da computação em nuvem da AWS

- Agilidade

A nuvem oferece acesso fácil a uma grande variedade de tecnologias para que você possa inovar mais rapidamente e criar praticamente tudo o que puder imaginar. **Você pode gerar rapidamente recursos conforme a necessidade**, de serviços de infraestrutura, como computação, armazenamento e bancos de dados até Internet das Coisas, machine learning, data lakes, análises de dados e muito mais.

**Você pode implantar serviços de tecnologia em questão de minutos** e passar da ideia à implementação com agilidade várias ordens de grandeza maior do que antes. Assim, você tem a liberdade de experimentar, testar novas ideias para diferenciar as experiências dos clientes e transformar a sua empresa.

- Elasticidade

Com a computação em nuvem, você não precisa provisionar recursos em excesso para absorver picos de atividades empresariais no futuro. Em vez disso, **você provisiona a quantidade de recursos realmente necessária**. Você pode aumentar ou diminuir instantaneamente a escala desses recursos para ajustar a capacidade de acordo com a evolução das necessidades empresariais.

- Economia de custo

A nuvem permite que você troque as despesas de capital (CapEx) (datacenters, servidores físicos etc.) por despesas variáveis (OpEx) **e pague apenas pela TI *consumida***. Além disso, as despesas variáveis são muito menores do que as que você pagaria por conta própria devido às economias de escala.

- Implantação global em questão de minutos

**Com a nuvem, você pode ampliar as atividades para novas regiões geográficas e implantar globalmente em minutos**. Por exemplo, a AWS tem infraestrutura em todo o mundo, o que permite que você implante aplicativos em vários locais físicos com apenas alguns cliques. Aproximar os aplicativos dos usuários finais reduz a latência e melhora a experiência desses usuários.

### Pilha de serviços da AWS

![alt text](./amazon-web-services-global-infrastructure-resized-600.png "Stack Services AWS")

- Fonte: <https://vmtoday.com/wp-content/uploads/sites/11/2013/08/amazon-web-services-global-infrastructure-resized-600.png>

### AWS Foundation Services

![alt text](./aws-foundation-services.png "Foundations Services")

- Fonte: Slides AWS Academy

### Serviços não gerenciados vs. serviços gerenciados

- Não gerenciados:
  - Dimensionamento, a tolerância a falhas e a disponibilidade são gerenciados por você.

- Gerenciados:
  - Dimensionamento, a tolerância a falhas e a disponibilidade geralmente são incorporadas ao serviço. Como exemplo os serviços do tipo PaaS abaixo.

![alt text](./aws-platform-services.png "Platform Services")

- Fonte: Slides AWS Academy

## 2. Cenários de nuvem

## 3. Visão Geral da Infraestrutura

## 4. Introdução aos serviços básicos da AWS

## 5. Wrap-up

## Fontes

- [AWS - What is Cloud Computing](https://aws.amazon.com/pt/what-is-cloud-computing//)
- [Introduction to Amazon Web Services - AWS](https://vmtoday.com/2013/07/introduction-to-amazon-web-services-aws/)

# Notas rápidas

- O **AWS Global Accelerator** aloca dois endereços IP estáticos IPv4 anycast e mantém o tráfego dentro da rede AWS globalmente redundante.

- O **AWS ParallelCluster** e o sistema de arquivos Amazon FSx são adequados para cargas de trabalho HPC na AWS e o ponto principal é a oportunidade de manter o controle sobre a infraestrutura. Isso é possível com o AWS ParallelCluster, pois é considerado uma ferramenta de gerenciamento de cluster de auto serviço.

- O **Amazon FSx for Luster** tem integração nativa com o Amazon S3.

- o **Amazon FSx para Windows File Server** não é um sistema de arquivos compatível com POSIX.

- O **Amazon FSx para Windows File Server** oferece suporte ao acesso entre VPCs, contas e regiões por meio de conexão direta ou VPN (local) e VPC Peering ou AWS Transit Gateway.

- Pode existir várias políticas **target tracking scaling** para um grupo de Auto Scaling, desde que cada uma delas use uma métrica diferente.

- Pode exitir várias **scaling policies** em vigor ao mesmo tempo.

- Ao usar a **step scaling policy** ou **target tracking policy**, as instâncias não são contadas para as métricas do grupo Auto Scaling **se um warmup de instância estiver em andamento**.

- Uma exceção aos **CloudWatch Alarms** que acionam ações apenas quando o estado do alarme muda, são os alarmes associados às ações do Amazon EC2 Auto Scaling.

- Um **cluster placement group** fornece rede de alto desempenho.

- Um **spread placement group** é mais apropriado para alta disponibilidade.

- Um **partition placement group** é usado ​​para implementar grandes cargas de trabalho distribuídas e replicadas.

- *É possível migrar* instâncias entre **placement groups**, mas a *mesclagem* de placement groups *não é suportada*.

- O **Classic Load Balancer** não suporta mapeamento dinâmico. Com o Classic Load Balancer, você deve definir estaticamente os mapeamentos de porta em uma instância de contêiner.

- O **Amazon Athena** é usado para executar consultas ad-hoc para analisar dados no S3.

- O Athena pode se integrar ao **Amazon QuickSight** que visualiza os dados por meio de painéis.

- O **Route 53** roteia automaticamente o tráfego para os novos endereços IP ELB. Com os registros `Alias`, os usuários não precisam alterar os conjuntos de registros, mesmo que os destinos tenham algumas alterações de configuração.

- O **AWS CloudTrail** não dispara eventos ou fornece notificações.

- O **AWS CloudTrail** criptografa por padrão os arquivos de log's de eventos gravados no S3 com **SSE**.

- O **CloudWatch Events** pode gerar eventos para ações no AWS Organization. Você pode registrar um tópico SNS para acionar uma notificação em uma regra de evento do CloudWatch.

- **Lifecycle Hooks** para os **Auto Scaling Groups** coloca a instância em *estado de espera antes do encerramento*. Durante esse estado de espera, você pode executar atividades personalizadas para recuperar dados operacionais críticos de uma instância com estado. O período de espera padrão é 1 hora.

- O **AWS CloudFormation Drift Detection** pode ser usado para detectar alterações feitas nos recursos da AWS fora dos modelos do CloudFormation.

- O **AWS CloudFormation Drift Detection** verifica apenas os valores de propriedade que são *explicitamente* definidos por modelos de pilha ou especificando parâmetros de modelo.

- O **Storage Gateway** é executado em cached ou stored mode.

  - No **cached mode**, seus dados primários são gravados no S3, enquanto retém os dados acessados com frequência localmente em um cache para acesso de baixa latência.

  - No **stored mode**, seus dados primários são armazenados localmente e todo o conjunto de dados está disponível para acesso de baixa latência durante o backup de forma assíncrona para a AWS.

- Com o Elastic Fabric Adapter (EFA), os usuários podem obter um melhor desempenho em comparação com a enhanced networking (Elastic Network Adapter) ou a Elastic Network Interface.

  - Um Elastic Fabric Adapter (EFA) é um dispositivo de rede que pode conectar à instância do Amazon EC2 para acelerar a computação de alto desempenho (HPC) e os aplicativos de aprendizado de máquina.
  - Um EFA é um Elastic Network Adapter (ENA) com recursos adicionais.

- Ao habilitar o **Streams do DynamoDB** em uma tabela, associe o ARN do stream a um **Lambda Function**. Imediatamente após um item da tabela ser modificado, um novo registro aparece no *stream* da tabela, acionando de forma síncrona a Lambda Function.

- A **Read Replica** deve ser usada para compartilhar a carga de trabalho de leitura da instância de banco de dados de origem. A **Read Replica** também pode ser configurada em uma região *diferente* da AWS.

- **Federation** é usado para *autenticar* usuários, não para *autorizar* serviços.

- As instâncias RDS não criptografadas existentes e seus instantâneos não podem ser criptografados. A **criptografia** para uma instância de **BD RDS** só pode ser **aplicada durante a sua criação**.

- O **Amazon Redshift Enhanced VPC Routing**, força todo o tráfego *COPY* e *UNLOAD* entre seu cluster e seus repositórios de dados por meio da sua *VPC*.

- Se o **Enhanced VPC Routing** não estiver habilitado, o tráfego será roteado pela *Internet*, incluindo o tráfego para outros serviços na rede AWS.

- As **Service Control Policies** são aplicadas a todos os usuários nas contas-membro, *incluindo contas root* em cada uma das contas.

- O **Gateway VPC Endpoint** fornece acesso privado e seguro ao *Amazon S3* e ao *DynamoDB* sem roteamento de tráfego pela Internet.

- O **AWS PrivateLink** fornece acesso privado e seguro a vários serviços da AWS *adicionando* uma **Elastic Network Interface (ENI)** a um VPC.

- Para compartilhar recursos da AWS atraves do **AWS Resource Access Manager (RAM)**, o compartilhamento precisa ser ativado com a *conta master* da AWS Organization.

- **Cognito** oferece suporte a provedores de identidade social, incluindo OIDC, SAML e seus próprios pools de identidade.

- Se você executa cargas de trabalho compatíveis com **PCI** ou **HIPAA** com base no *Modelo de Responsabilidade Compartilhada da AWS*, recomendamos que você registre seus dados de uso do **CloudFront** nos últimos *365 dias* para fins de auditoria futura.

- Os objetos no **Glacier Deep Archive** *não podem ser movidos diretamente* para outra classe de armazenamento. **Eles precisam ser restaurados primeiro** e depois copiados para a classe de armazenamento desejada.

- O **Amazon S3 Glacier Select** pode consultar dados *diretamente* do Amazon S3 Glacier e a *restauração* de dados para o bucket S3 *não é necessária* para consultar esses dados.

- O **Amazon S3 Select** pode ser usado para consultar um subconjunto de dados nos formatos **CSV, JSON ou Parquet**. A compactação **GZIP e BZIP2** é compatível com o formato **CSV ou JSON** com *server-side encryption*.

- Com o **AWS Managed Microsoft AD** seus usuários podem usar os recursos de nuvem da AWS bem como serviços em seu ambiente local, após implementar a *VPN* ou *Direct Connect*.

- Um **Alias Record** no **Route 53** contém *um ponteiro* para uma distribuição do *CloudFront*, um ambiente *Elastic Beanstalk*, um *ELB Classic*, *Application ou Network Load Balancer*, um bucket do *Amazon S3* que é configurado como um site estático, *ou outro registro Route 53* na mesma zona hospedada.

- O **Alias** pode ser criado ​​para o *domínio raiz* e o *subdomínio*, enquanto que o registro **CNAME** pode ser criado *apenas para o subdomínio*.

- A **Vault Lock Policy** é usada para impedir que os usuários realizem ações específicas (por ex. apagar) em arquivos, para fins de conformidade e pode ser modificada *apenas uma vez*.

- A **Vault Access Policy** é usada para conceder permissão aos arquivos e pode ser modificada de acordo com a necessidade do usuário.

- **EC2 hibernate**:
  - *Não é compatível* com *Auto-Scaling Groups*.
  - *Não é compatível* com *volume de armazenamento da instância*.

- **EC2 hibernate**:
  - *Requer* volumes root como **Amazon EBS**.

- Após a **hibernação** ou **Stopped and Started** das instâncias EC2:
  - O *IP público **é liberado***.
  - O *IP privado* e o *Elastic IP* **são retidos**.

- O **AWS Direct Connect** não envolve a Internet, em vez disso, ele usa conexões de rede privadas dedicadas entre sua intranet e o Amazon VPC.

- Para *conectar* o ambiente local com a AWS *via Internet* a solução é usar uma **VPN**.

- Com o **AWS Serverless Application Model (SAM)** o aplicativo pode ser *testado localmente* invocando a função Lambda e as fontes de eventos localmente.

- O **CodeDeploy** é parte integrante do AWS SAM, que pode ajudar a implantar gradualmente dentro da nuvem junto com os aplicativos existentes.

- O **Simple AD** é um *Microsoft Active Directory–compatible directory* do **AWS Directory Service**, que pode ser usado como um diretório independente na nuvem para:
  - Suportar cargas de trabalho do Windows que precisam de recursos AD básicos ou aplicativos AWS compatíveis; e
  - Suportar cargas de trabalho Linux que precisam de serviço LDAP.

- O **AWS OpsWorks Stacks** permite que você gerencie aplicativos e *servidores na AWS* e *on-premises*, através de *pilhas* (Stacks) com diferentes *camadas*, por ex. como balanceamento de carga, banco de dados e servidor de aplicativos.

- O **Cross-Origin Resource Sharing (CORS)** garante que os scripts e outros conteúdos ativos carregados de um site ou domínio não possam interferir ou interagir com o conteúdo de outro local sem uma indicação explícita de que este é o comportamento desejado.

- O **Cross-Origin Resource Sharing (CORS)** suporta apenas os métodos GET, PUT, POST, DELETE e HEAD.

- Uma conta de membro **não pode ser migrada** entre **AWS Organization** diretamente, precisa ser *removida* da antiga AWS Organization e *depois aceitar o convite* na AWS Organization nova.

- O **AWS Fargate** permite que você execute seus aplicativos em contêineres sem a necessidade de provisionar e gerenciar a infraestrutura de back-end.

- As *tabelas globais* do **Amazon DynamoDB** fornecem uma solução para implantar um banco de dados multi-region e multi-master.

- O **object lock** do **Amazon S3** deve ser habilitado para armazenar objetos usando o modelo 'Write Once, Read Many' (WORM), você pode evitar que os objetos S3 sejam excluídos ou sobrescritos por um determinado período de tempo ou indefinidamente.

- **S3 Glacier Deep Archive** não apaga objetos automaticamente.

- O **AWS Glue** mantém um registro dos dados processados ​​usando o **Job Bookmark**, com isso *apenas as alterações desde o último marcador* serão processadas.

- Para cada túnel VPN, a AWS fornece *dois endpoints VPN* diferentes. **ECMP (Equal Cost Multi Path)** pode ser usado para transportar tráfego em ambos os terminais VPN, o que pode aumentar o desempenho e a transferência de dados mais rápida.

- Para aplicativos em que a utilização do banco de dados *não pode ser prevista*, o **Amazon DynamoDB** pode ser usado com o **Auto Scaling**. O escalonamento automático *precisa ser* aplicado à tabela do DynamoDB *junto com* o **Global Secondary Index**, que usa capacidade de leitura / gravação separada.

- O **AWS Kinesis Streams** *não pode carregar* dados diretamente para o **Amazon Redshift**.

- O **Amazon Kinesis Firehose** é a opção para *carregar dados* em ferramentas analíticas (como o **Amazon Redshift**) de várias fontes.

- **SQS FIFO queue** usa Message Deduplication ID e Message Group ID por mensagem.
  - O **Message Deduplication ID** de mensagens é usado como um token durante o envio de mensagens.
  - A **Message Group ID** é usada como uma etiqueta, com base em vários grupos, para que as mensagens em um grupo sejam processadas de maneira ordenada.

- O **AWS Batch Job** envia informações de log para logs do CloudWatch, o que *requer* que o driver de log awslogs *seja configurado* em recursos de computação com AMI personalizado.
  - No caso de **Amazon ECS-optimized AMI**, esses drivers *são pré-configurados*.

- Os **volumes EBS** anexados à instância EC2 *sempre terão* que permanecer na *mesma* zona de disponibilidade que a instância EC2.

- **Elastic Beanstalk** vs **ECS** realmente se resume ao controle.
  - O **ECS** lhe dará controle, você precisará especificar o tamanho e o número de nós no cluster e se o escalonamento automático deve ou não ser usado.
  - Com o **Elastic Beanstalk** você simplesmente fornece um Dockerfile e o Elastic Beanstalk cuida de tudo.

![alt text](./pipeline.png "Pipeline")

- Ao montar o **Amazon EFS**, se a criptografia de dados em trânsito estiver habilitada e o **EFS Mount Helper** inicializa o processo de *Stunnel* do cliente para criptografar os dados em trânsito.

- O **Network Load Balancer (NLB)** pode ser usado para *encerrar conexões TLS* em vez de uma instância de back-end, reduzindo a carga nestas instância. Para negociar conexões TLS com clientes o **NLB** usa uma *política de segurança que consiste em protocolos e cifras*.

- Na **AWS Managed Blockchain** quando qualquer novo membro é criado, um ID exclusivo é atribuído a esses membros. Para qualquer transação entre esses membros, cada membro deve usar o seguinte formato “**ResourceID**.**MemberID**.**NetworkID**.managedblockchain.AWSRegion.amazonaws.com:PortNumber”.

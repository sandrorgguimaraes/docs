# O que é o KMS (Key Management Service)

> Fontes.:
> [Visão Geral](https://aws.amazon.com/pt/kms/)
> [Guia do Desenvolvedor](https://docs.aws.amazon.com/kms/latest/developerguide/overview.html)

É um serviço que facilita a criação e o gerenciamento de chaves mestras do cliente (CMKs), as chaves de criptografia usadas para criptografar os dados, e o controle do seu uso em uma ampla variedade de serviços da AWS e em seus aplicativos.

É um serviço seguro e resiliente que usa módulos de segurança de hardware validados ou em processo de [validação pelo FIPS 140-2](https://csrc.nist.gov/projects/cryptographic-module-validation-program/Certificate/3139) para proteger suas chaves. 

Também é integrado ao [AWS CloudTrail](../../management_and_governance/cloudtrail/cloudtrail.md) para fornecer logs contendo toda a utilização das chaves para ajudar a cumprir requisitos normativos e de conformidade.

## Funcionalidades

- [Criar](https://docs.aws.amazon.com/kms/latest/developerguide/create-keys.html), [editar](https://docs.aws.amazon.com/kms/latest/developerguide/editing-keys.html) e [visualizar CMKs simétricos e assimétricos](https://docs.aws.amazon.com/kms/latest/developerguide/symmetric-asymmetric.html);
- [Ativar e desativar](https://docs.aws.amazon.com/kms/latest/developerguide/enabling-keys.html) CMKs;
- Crio. editar e visualizar as [principais políticas](https://docs.aws.amazon.com/kms/latest/developerguide/key-policies.html) e [concessões](https://docs.aws.amazon.com/kms/latest/developerguide/grants.html) para seus CMKs;
- Ativar e desativar a [rotação automática](https://docs.aws.amazon.com/kms/latest/developerguide/rotate-keys.html) do material criptográfico em um CMK;
- [Tageie seus CMKs](https://docs.aws.amazon.com/kms/latest/developerguide/tagging-keys.html) para identificação, automação e rastreamento de custos
- Crie, exclua, liste e atualize aliases, que são nomes amigáveis ​​para seus CMKs;
- [Exclua CMKs](https://docs.aws.amazon.com/kms/latest/developerguide/deleting-keys.html) para completar o ciclo de vida da chave;

## Operações criptográficas

- Criptografar, descriptografar e recriptografar dados com CMKs simétricos ou assimétricos;
- Assine e verifique as mensagens com CMKs assimétricos;
- Gere chaves de dados simétricos exportáveis ​​e pares de chaves de dados assimétricos;
- Gere números aleatórios adequados para aplicativos criptográficos;

## Recursos avançados

- [Importar material criptográfico](https://docs.aws.amazon.com/kms/latest/developerguide/importing-keys.html) em um CMK;
- Crie CMKs em seu próprio [armazenamento de chaves personalizado](https://docs.aws.amazon.com/kms/latest/developerguide/custom-key-store-overview.html) apoiado por um cluster AWS CloudHSM;
- Conecte-se diretamente ao AWS KMS por meio de um [endpoint privado em seu VPC](https://docs.aws.amazon.com/kms/latest/developerguide/kms-vpc-endpoint.html);
- Use [TLS pós-quântico híbrido](https://docs.aws.amazon.com/kms/latest/developerguide/pqtls.html) para fornecer criptografia prospectiva em trânsito para os dados que você envia para AWS KMS;

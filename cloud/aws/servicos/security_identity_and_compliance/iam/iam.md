# O que é o IAM

O AWS Identity and Access Management (IAM) é um serviço da web que ajuda você a controlar o acesso aos recursos da AWS de forma segura. Você usa o IAM para controlar quem é [autenticado](#authentication) (fez login) e [autorizado](#authorization) (tem permissões) a usar os [recursos](#resources).

Ao criar uma conta da AWS, você começa com uma única identidade de login que tenha acesso total a todos os recursos e serviços da AWS na conta. Essa identidade é chamada de AWS da conta da usuário root e é acessada pelo login com o endereço de e-mail e a senha que você usou para criar a conta. Recomendamos que não use o usuário raiz para suas tarefas diárias, nem mesmo as administrativas. Em vez disso, siga as [melhores práticas de uso do usuário root somente para criar seu primeiro usuário do IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#create-iam-users). Depois, armazene as credenciais usuário raiz com segurança e use-as para executar apenas algumas tarefas de gerenciamento de contas e de serviços.

> Fonte.:
>
> - <https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/introduction.html>

## Recursos do IAM

- Acesso compartilhado à sua conta da AWS
- Permissões granulares
    > Você pode conceder permissões diferentes a pessoas diferentes para diferentes recursos.
- Acesso seguro a recursos da AWS para aplicativos que são executados no Amazon EC2
- Autenticação multifator (MFA)
- Federação de identidades
    > Você pode permitir que os usuários que já têm senhas em outro lugar — por exemplo, em sua rede corporativa ou com um provedor de identidade de Internet — obtenham acesso temporário à sua conta da AWS.
- Informações de identidade para garantia
- Compatibilidade com PCI DSS
    > IAM oferece suporte a processamento, armazenamento e transmissão de dados de cartão de crédito por um comerciante ou provedor de serviços, e foi validado como em conformidade com o Data Security Standard (DSS – Padrão de segurança de dados) da Payment Card Industry (PCI – Setor de cartão de crédito).
- Integrado com muitos serviços da AWS
- Eventualmente consistente
    > O IAM, como muitos outros serviços da AWS, é eventualmente consistente. O IAM atinge alta disponibilidade replicando dados entre vários servidores nos datacenters da Amazon ao redor do mundo. A alteração deverá ser replicada em todo o IAM, o que pode levar algum tempo.
- Uso gratuito
    > O AWS Identity and Access Management (IAM) e o AWS Security Token Service (AWS STS) são recursos da sua conta da AWS oferecidos sem custos adicionais.

## Como funciona o IAM

O IAM fornece a infraestrutura necessária para controlar a autenticação e autorização da sua conta.

A infraestrutura do IAM inclui os seguintes elementos:

![Infraestrutura do IAM](./intro-diagram_policies_800.png "Infraestrutura do IAM")

> Fonte.:
>
> - <https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/images/intro-diagram%20_policies_800.png>

### Principal

Um **principal** é uma pessoa ou um aplicativo que pode fazer uma solicitação de uma ação ou operação em um recurso da AWS. Como melhor prática, não use suas credenciais de usuário root para seu trabalho diário. Em vez disso, crie [identidades do IAM ](https://docs.aws.amazon.com/IAM/latest/UserGuide/id.html) (usuários, grupos, funções ou credenciais temporárias). Você também pode oferecer suporte a usuários federados ou a acesso programático para permitir que um aplicativo acesse sua conta da AWS.

### Request

Quando uma entidade principal tenta usar o Console de gerenciamento da AWS, a API da AWS ou a AWS CLI, ela envia uma solicitação para a AWS. A solicitação inclui as seguintes informações:

- Ações (ou operações) –> As ações ou as operações que o [principal](#principal) deseja executar. Pode ser uma ação no Console de gerenciamento da AWS, uma operação na AWS CLI ou na API da AWS.
- Recursos –> O objeto de recurso da AWS no qual as ações ou operações são executadas.
- [principal](#principal) –> A pessoa ou o aplicativo que usou uma entidade (usuário ou função) para enviar a solicitação. As informações sobre o [principal](#principal) incluem as políticas associadas à entidade usada pelo [principal](#principal) para fazer login.
- Dados do ambiente –> Informações sobre o endereço IP, o agente de usuário, o status do SSL habilitado ou a hora do dia.
- Dados do recurso –> Dados relacionados ao recurso que está sendo solicitado. Isso pode incluir informações como um nome da tabela do DynamoDB ou uma tag em uma instância do Amazon EC2.

#### Contexto de Solicitação

O AWS reúne as informações da solicitação / [request](#request) em um **contexto de solicitação**, que é usado para avaliar e autorizar a solicitação.

### Authentication

Uma entidade [principal](#principal) deve ser autenticada (conectado na AWS) usando suas credenciais para enviar uma solicitação para a AWS. *Alguns serviços, como o Amazon S3 e o AWS STS, permitem algumas solicitações de usuários anônimos. No entanto, eles são a exceção à regra*.

Para autenticar-se no console como um usuário root, você deve fazer login com seu endereço de e-mail e senha. Como usuário do IAM, fornece o ID da conta ou o alias e usa seu nome de usuário e senha. Para autenticar-se na API ou na AWS CLI, você deve fornecer sua chave de acesso e chave secreta. Também pode ser necessário fornecer informações adicionais de segurança. Por exemplo, a AWS recomenda o uso da autenticação multifator (MFA) para aumentar a segurança de sua conta.

### Authorization

Você também deve ser autorizado (permitido) para concluir a solicitação. Durante a autorização, a AWS usa valores do [contexto da solicitação](#contexto-de-solicitação) para verificar a existência de políticas que se aplicam à solicitação.

#### Lógica da avaliação das Políticas

![Lógica avaliação das Políticas](./PolicyEvaluationHorizontal.png "Lógica avaliação das Políticas")

> Fonte.:
>
> - <https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/reference_policies_evaluation-logic.html>
> - <https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/images/PolicyEvaluationHorizontal.png>

### Ações ou operações

Depois que sua solicitação tiver sido [autenticada](#authentication) e [autorizada](#authorization), a AWS aprovará as ações ou operações em sua solicitação. As operações são definidas por um serviço e incluem coisas que você pode fazer em um recurso, como visualizar, criar, editar e excluir esse recurso. Por exemplo, *o IAM oferece suporte a aproximadamente 40 ações para um recurso de usuário* incluindo as seguintes ações:

- CreateUser
- DeleteUser
- GetUser
- UpdateUser

Para permitir que uma entidade principal execute uma operação, você deve incluir as ações necessárias em uma política aplicável à entidade principal ou ao recurso afetado.

### Resources

Depois que a AWS aprova as operações em sua solicitação, elas podem ser executadas nos recursos relacionados em sua conta. Um **recurso** é uma objeto que existe dentro de um serviço. Os exemplos incluem uma instância do Amazon EC2, um usuário do IAM e um bucket do Amazon S3.

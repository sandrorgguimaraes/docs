# O que é o RDS (Relational Database Service)

> TODO: A fazer...
>
> Detalhar.:
> Read Replica should be used to share the read workload of the source DB instance. Read Replica can also be configured in a different AWS region. Refer to <https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ReadRepl.html>.
>
> - a Multi-AZ instance cannot be promoted to be a Read Replica
> - a Read Replica can be launched in another region for RDS MySQL.

# O que é o Auto Scaling Group

> Fonte.: https://docs.aws.amazon.com/autoscaling/ec2/userguide/what-is-amazon-ec2-auto-scaling.html


O Amazon EC2 Auto Scaling ajuda a garantir que tenha o número correto de instâncias do Amazon EC2 disponíveis para lidar com a carga de seu aplicativo.

Você cria coleções de instâncias EC2, chamadas **grupos de Auto Scaling** e pode especificar o número mínimo, máximo e desejado de instâncias em cada grupo do Auto Scaling

![Diagrama Básico Auto Scaling](./as-basic-diagram.png "Diagrama Básico Auto Scaling")

## Principais componentes

### Grupos

Onde as instâncias são organizadas para que possam ser tratadas como uma **unidade lógica** para fins de dimensionamento e gerenciamento. Ao criar um grupo, você pode especificar o número mínimo, máximo e desejado de instâncias EC2.

### Modelos de configuração

Onde é especificado informações como AMI ID, tipo de instância, par de chaves, grupos de segurança e mapeamento de dispositivo de bloco.

### Opções de escala

O Amazon EC2 Auto Scaling oferece várias maneiras de dimensionar seu grupo de Auto Scaling.

- Manter os níveis de instância atuais em todos os momentos
- Escale manualmente
- Escala com base em uma programação
- Escala com base na demanda
- Use escalonamento preditivo

#### Política de dimensionamento

Instrui o Amazon EC2 Auto Scaling a rastrear uma métrica CloudWatch específica e define a ação a ser tomada quando o alarme CloudWatch associado está em **ALARME**.

O Amazon EC2 Auto Scaling oferece suporte aos seguintes tipos de políticas de dimensionamento:

- **Escala de rastreamento de destino**: Aumente ou diminua a capacidade atual do grupo com base em um valor de destino para uma métrica específica. Isso é semelhante à maneira como o termostato mantém a temperatura da sua casa - você seleciona uma temperatura e o termostato faz o resto.
- **Escala de etapas**: Aumente ou diminua a capacidade atual do grupo com base em um conjunto de ajustes de escala, conhecidos como ajustes de etapas, que variam com base no tamanho da violação do alarme.
- **Escala simples**: Aumente ou diminua a capacidade atual do grupo com base em um único ajuste de escala.

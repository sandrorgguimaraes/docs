# O que é Placement Groups ou Grupos de Posicionamento

> Fonte.: <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html>

Quando você inicia uma nova instância EC2, o serviço EC2 tenta distribuir de forma que todas as suas instâncias sejam **espalhadas** pelo hardware subjacente para minimizar as falhas correlacionadas.

Você pode usar os placement groups (grupos de posicionamento) para influenciar o posicionamento de um grupo de instâncias **interdependentes** para atender às necessidades de sua carga de trabalho.

## Estratégias disponíveis

### Cluster

Empacota as instâncias juntas dentro de uma zona de disponibilidade. Essa estratégia permite que as cargas de trabalho alcancem o desempenho de rede de baixa latência necessário para a comunicação entre nós fortemente acoplada, típica de aplicativos HPC.

![Placement Group Cluster](./placement-group-cluster.png "Placement Group Cluster")

### Partição

Distribui suas instâncias por partições lógicas de forma que grupos de instâncias em uma partição não compartilhem o hardware subjacente com grupos de instâncias em partições diferentes. Essa estratégia é normalmente usada por grandes cargas de trabalho distribuídas e replicadas, 
como Hadoop, Cassandra e Kafka.

![Placement Group Partition](./placement-group-partition.png "Placement Group Partition")

### Spread

Coloca estritamente um pequeno grupo de instâncias em hardware subjacente distinto para reduzir falhas correlacionadas.

![Placement Group Spread](./placement-group-spread.png "Placement Group Spread")

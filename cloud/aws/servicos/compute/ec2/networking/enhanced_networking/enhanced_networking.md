# O que é Enhanced networking ou Rede Aprimorada

A Enhanced networking usa virtualização de E/S root única (SR-IOV) para fornecer recursos de rede de alto desempenho em [tipos de instância com suporte](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/enhanced-networking.html#supported_instances).

SR-IOV é um método de virtualização de dispositivo que fornece maior desempenho de E/S e menor utilização da CPU quando comparado às interfaces de rede virtualizadas tradicionais.

A rede aprimorada fornece maior largura de banda, desempenho de pacote por segundo (PPS) mais alto e latências entre instâncias consistentemente mais baixas.

Não há cobrança adicional pelo uso de rede avançada.

## Tipos

Dependendo do seu tipo de instância, a rede avançada pode ser ativada usando um dos seguintes mecanismos:

### Elastic Network Adapter (ENA)

O Elastic Network Adapter (ENA) suporta velocidades de rede de até 100 Gbps para tipos de instância suportados.

Os tipos de instância da geração atual suportam ENA para rede aprimorada, exceto as instâncias M4 menores que m4.16xlarge.

### Intel 82599 Virtual Function (VF) interface

A interface Intel 82599 Virtual Function oferece suporte a velocidades de rede de até 10 Gbps para os tipos de instância suportados.

Os seguintes tipos de instância usam a interface Intel 82599 VF para rede avançada: C3, C4, D2, I2, M4 (excluindo m4.16xlarge) e R3.

> Para obter informações sobre a velocidade de rede com suporte para cada tipo de instância, consulte [Tipos de instância do Amazon EC2](https://aws.amazon.com/ec2/instance-types).

## Habilitando rede aprimorada em sua instância

- Se o seu tipo de instância for compatível com o **Elastic Network Adapter**, siga os procedimentos em [Ativando a rede aprimorada com o Elastic Network Adapter (ENA) em instâncias do Linux](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/enhanced-networking-ena.html).
- Se o seu tipo de instância for compatível com a interface **Intel 82599 VF**, siga os procedimentos em [Ativando rede aprimorada com a interface Intel 82599 VF em instâncias do Linux](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/sriov-networking.html).

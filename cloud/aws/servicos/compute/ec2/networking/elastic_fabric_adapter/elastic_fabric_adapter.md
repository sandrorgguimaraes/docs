# O que é Elastic Fabric Adapter

Um Elastic Fabric Adapter (EFA) é um dispositivo de rede que você pode conectar à sua instância do Amazon EC2 para acelerar a computação de alto desempenho (HPC) e os aplicativos de aprendizado de máquina.

O EFA permite que você alcance o desempenho do aplicativo de um cluster HPC local, com a escalabilidade, flexibilidade e elasticidade fornecidas pela Nuvem AWS.

O EFA fornece latência mais baixa e mais consistente e maior rendimento do que o transporte TCP tradicionalmente usado em sistemas HPC baseados em nuvem.

Ele aprimora o desempenho da comunicação entre instâncias, que é crítica para o dimensionamento de HPC e aplicativos de aprendizado de máquina.

Ele é otimizado para funcionar na infraestrutura de rede AWS existente e pode escalar dependendo dos requisitos do aplicativo.

## EFA Básico

Um EFA é um [Elastic Network Adapter (ENA)](../enhanced_networking/enhanced_networking.md) com recursos adicionais. Ele fornece todas as funcionalidades de um ENA, com uma funcionalidade adicional de desvio do sistema operacional.

OS-bypass é um modelo de acesso que permite que os aplicativos HPC e de aprendizado de máquina se comuniquem diretamente com o hardware de interface de rede para fornecer funcionalidade de transporte confiável de baixa latência.

![Elastic Fabric Adapter Stack](./efa_stack.png "Elastic Fabric Adapter Stack")

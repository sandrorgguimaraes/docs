# O que é o EC2 (Elastic Compute Cloud)

O Amazon Elastic Compute Cloud (Amazon EC2) é um web service que disponibiliza capacidade computacional segura e redimensionável na [nuvem](../../../certificacoes/arquiteto/modulo_00.md#1-introdução-à-nuvem-da-aws). Ele foi projetado para facilitar a computação em nuvem na escala da web para os desenvolvedores. A interface de web service simples permite configurar a capacidade com o mínimo de esforço, oferecendo controle total dos recursos computacionais.

## Recursos

> Fonte.: <https://aws.amazon.com/pt/ec2/features/>

### Instâncias sem sistema operacional

As instâncias sem sistema operacional do Amazon EC2 oferecem aos aplicativos acesso direto ao processador e à memória do servidor subjacente. são ideais para cargas de trabalho que exigem acesso a conjuntos de recursos de hardware (como Intel® VT-x) ou para aplicativos que precisam ser executados em ambientes não virtualizados para cumprir requisitos de licenciamento ou suporte.

### Otimização de Performance e Custo com o EC2 Fleet

Com uma **única** chamada de API, permite provisionar instâncias EC2 com capacidade computacional diversa, em várias zonas de disponibilidade e modelos de compra para ajudar a otimizar a escala, a performance e o custo.

Saiba mais [aqui](https://aws.amazon.com/pt/about-aws/whats-new/2018/04/introducing-amazon-ec2-fleet/) e como usar [aqui](https://aws.amazon.com/pt/blogs/aws/ec2-fleet-manage-thousands-of-on-demand-and-spot-instances-with-one-request/).

### Pause e retome suas instâncias

Você pode hibernar suas instâncias do Amazon EC2 com base no Amazon EBS e reiniciá-las do mesmo estado posteriormente. Enquanto a instância estiver hibernando, você não precisará pagar pela utilização da instância. O armazenamento é pago de acordo com as taxas padrão do EBS.

Para saber mais acesse [aqui](https://aws.amazon.com/ec2/faqs/) e procure por 'Hibernate'.

### Instâncias com GPU Computacionais

Para quem precisa de capacidade massiva de processamento de ponto flutuante.

### Instâncias com GPU Gráficas

Para quem precisa de alta capacidade gráfica.

### Instâncias de E/S elevada

Para quem precisa de alta velocidade, baixa latência e acesso aleatório aos dados.

### Instâncias de armazenamento denso HDD

Para quem precisa de armazenamento massivo e acesso sequencial aos dados.

### Configurações de CPU otimizada

Este recurso possibilita aumentar o controle sobre as instâncias EC2 em duas frentes:

1. Especificar um número personalizado de vCPUs ao iniciar novas instâncias para reduzir custos de licenciamento baseados em vCPUs;
2. Desabilitar o recurso de multithread para cargas de trabalho que executam bem em CPUs de um único thread, como alguns aplicativos de computação de alta performance (HPC);

### Opções de armazenamento flexíveis

Além do armazenamento embutido nas instâncias, também é possível usar o Amazon Elastic Block Store (Amazon EBS) e o Amazon Elastic File System (Amazon EFS) para atender a outros requisitos de armazenamento.

### Pagamento de acordo com a utilização

Será cobrado ao final de cada mês somente pelos recursos do EC2 que realmente forem utilizados. Horas parciais são cobradas como horas inteiras para instâncias do Windows e por segundo para as instâncias do Linux.

### Vários locais

Os locais do Amazon EC2 são compostos pelas Regiões e pelas Zonas de Disponibilidade.

### Endereços IP elásticos

Os endereços IP elásticos são endereços IP estáticos projetados para computação em nuvem dinâmica. Um endereço Elastic IP está associado à sua conta e não a uma instância específica e você controla esse endereço até que decida explicitamente liberá-lo.

### Amazon EC2 Auto Scaling

O Amazon EC2 Auto Scaling permite a ampliação ou a redução automática da capacidade de seu Amazon EC2 de acordo com as condições definidas.

### Cluster de Computação de alta performance (HPC)

Complexas cargas de trabalho computacionais, assim como processos paralelos fortemente acoplados ou com aplicativos sensíveis ao desempenho da rede, podem alcançar o mesmo alto desempenho computacional e de rede fornecido por uma infraestrutura personalizada, enquanto se beneficiam das vantagens de flexibilidade, elasticidade e de custo do Amazon EC2.

### Redes aperfeiçoadas

As redes aperfeiçoadas permitem obter uma performance de pacotes por segundo (PPS) significativamente mais alta, menor variação de rede e latências mais baixas.

### Elastic Fabric Adapter (interconexão rápida para clusters de HPC)

O Elastic Fabric Adapter (EFA – Adaptador de malha elástica) é uma interface de rede que possibilita executar aplicativos HPC que exigem altos níveis de comunicação entre instâncias em grande escala na AWS.

### Disponível no AWS PrivateLink

Permite acessar de modo privado APIs do Amazon EC2 por meio da Amazon Virtual Private Cloud (VPC) ou do AWS Direct Connect, sem usar IPs públicos e sem precisar que o tráfego passe pela Internet. Para usar o Amazon EC2 com o AWS PrivateLink, é necessário criar um endpoint para o EC2 na VPC.

### Amazon Time Sync Service

O Amazon Time Sync Service oferece uma fonte de horário altamente precisa, confiável e disponível para os serviços da AWS, inclusive para as instâncias do EC2.

# O que é AWS Lambda

O AWS Lambda permite que você execute código sem provisionar ou gerenciar servidores. Você paga apenas pelo tempo de computação que consome.

Com o Lambda, você pode executar código para praticamente qualquer tipo de aplicativo ou serviço de back-end - tudo sem administração. Basta fazer upload de seu código e o Lambda cuidará de tudo o que for necessário para executar e escalar seu código com alta disponibilidade. Você pode configurar seu código para acionar automaticamente a partir de outros serviços da AWS ou chamá-lo diretamente de qualquer aplicativo da web ou móvel.

## Como funciona

![AWS Lambda](./Diagram_Lambda-HowItWorks.png "AWS Lambda")

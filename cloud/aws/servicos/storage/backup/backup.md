# O que é o AWS Backup

É um serviço de backup totalmente gerenciado que facilita centralizar e automatizar o backup de dados nos serviços da AWS.

Usando o AWS Backup, você pode configurar centralmente as políticas de backup e monitorar a atividade de backup para recursos da AWS, como volumes do [Amazon EBS](../../storage/../compute/ec2/ebs/ebs.md), instâncias do [Amazon EC2](../../compute/ec2/ec2.md), bancos de dados [Amazon RDS](../../databases/rds/rds.md), tabelas do [Amazon DynamoDB](../../databases/dynamodb/dynamodb.md), sistemas de arquivos [Amazon EFS](../../storage/efs/efs.md) e volumes do [AWS Storage Gateway](../../storage/storage_gateway/storage_gateway.md).

O AWS Backup automatiza e consolida tarefas de backup anteriormente executadas serviço por serviço, eliminando a necessidade de criar scripts personalizados e processos manuais.

Com apenas alguns cliques no console do AWS Backup, você pode criar políticas de backup que automatizam os agendamentos de backup e o gerenciamento de retenção.

O AWS Backup fornece uma solução de backup baseada em políticas totalmente gerenciada, simplificando o gerenciamento de backup.

## Como funciona

![AWS Backup](./Diagram_cryo_how-it-works.png "AWS Backup")

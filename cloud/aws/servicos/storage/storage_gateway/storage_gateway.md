# O que é o AWS Storage Gateway

O AWS Storage Gateway é um serviço de armazenamento em nuvem híbrido que oferece acesso local a armazenamento em nuvem virtualmente ilimitado.

Os clientes usam o Gateway de armazenamento para simplificar o gerenciamento de armazenamento e reduzir custos para os principais casos de uso de armazenamento em nuvem híbrida.

Isso inclui mover backups para a nuvem, usando compartilhamentos de arquivos locais com o suporte de armazenamento em nuvem e fornecer acesso de baixa latência aos dados na AWS para aplicativos locais.

Para dar suporte a esses casos de uso, o Storage Gateway oferece três tipos diferentes de gateways:

- [File Gateway](https://aws.amazon.com/storagegateway/file/);
- [Tape Gateway](https://aws.amazon.com/storagegateway/vtl/) e;
- [Volume Gateway](https://aws.amazon.com/storagegateway/volume/);
 
Que conectam perfeitamente os aplicativos locais ao armazenamento em nuvem, armazenando os dados em cache localmente para acesso de baixa latência.

Seus aplicativos se conectam ao serviço por meio de uma máquina virtual ou dispositivo de hardware de gateway usando protocolos de armazenamento padrão, como NFS, SMB e iSCSI.

O gateway se conecta aos serviços de armazenamento da AWS, como [Amazon S3](../../storage/s3/s3.md), [Amazon S3 Glacier](../../storage/s3/s3.md#classes_de_armazenamento), [Amazon S3 Glacier Deep Archive](../../storage/s3/s3.md#classes_de_armazenamento), [Amazon EBS](../../storage/../compute/ec2/ebs/ebs.md) e [AWS Backup](../../storage/backup/backup.md), fornecendo armazenamento para arquivos, volumes, instantâneos e fitas virtuais na AWS.

O serviço inclui um mecanismo de transferência de dados eficiente e altamente otimizado, com gerenciamento de largura de banda e resiliência de rede automatizada.

## Como funciona

![Storage Gateway](./Storage-Gateway-How-it-Works-Diagram.png "Storage Gateway")

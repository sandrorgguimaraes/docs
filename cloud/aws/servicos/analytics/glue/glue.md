# O que é o AWS Glue

O AWS Glue é um serviço de extração, transformação e carregamento (ETL) totalmente gerenciado que torna mais fácil para os clientes preparar e carregar seus dados para análise.

Você pode criar e executar um trabalho ETL com alguns cliques no AWS Management Console.

Você simplesmente aponta o AWS Glue para seus dados armazenados na AWS, e o AWS Glue descobre seus dados e armazena os metadados associados (por exemplo, definição de tabela e esquema) no AWS Glue Data Catalog.

Depois de catalogados, seus dados são imediatamente pesquisáveis, consultáveis ​​e disponíveis para ETL.

## Como funciona

Selecione uma fonte de dados e um destino de dados. O AWS Glue irá gerar código ETL em Scala ou Python para extrair dados da fonte, transformar os dados para corresponder ao esquema de destino e carregá-los no destino. Você pode editar, depurar e testar este código por meio do Console, em seu IDE favorito ou em qualquer notebook.

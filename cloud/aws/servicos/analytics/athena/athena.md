# O que é o Amazon Athena

Amazon Athena é um serviço de consulta interativo que facilita a análise de dados no [Amazon S3](../../storage/s3/s3.md) usando SQL padrão.

O Athena não tem servidor, portanto, *não há infraestrutura para gerenciar* e *você paga apenas pelas consultas que executa*.

Athena é fácil de usar. Basta apontar para seus dados no Amazon S3, definir o esquema e começar a consultar usando SQL padrão.

A maioria dos resultados é entregue em segundos. Com o Athena, não há necessidade de tarefas complexas de ETL para preparar seus dados para análise. Isso torna mais fácil para qualquer pessoa com habilidades em SQL analisar rapidamente conjuntos de dados em grande escala.

O Athena está pronto para uso integrado ao [AWS Glue](../../analytics/glue/glue.md) Data Catalog, permitindo que você crie um repositório de metadados unificado em vários serviços, rastreie fontes de dados para descobrir esquemas e preencher seu Catálogo com definições de tabelas e partições novas e modificadas e manter o esquema controle de versão.
